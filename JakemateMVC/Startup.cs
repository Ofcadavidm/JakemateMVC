﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JakemateMVC.Startup))]
namespace JakemateMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
